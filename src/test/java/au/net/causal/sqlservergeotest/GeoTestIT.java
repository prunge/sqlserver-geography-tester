package au.net.causal.sqlservergeotest;

import com.microsoft.sqlserver.jdbc.Geography;
import com.microsoft.sqlserver.jdbc.ISQLServerPreparedStatement;
import com.microsoft.sqlserver.jdbc.ISQLServerResultSet;
import org.assertj.core.data.Offset;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.AssertionsForClassTypes.within;

class GeoTestIT
{
    private static final Offset<Double> DELTA = within(0.001);

    private static final int WGS_84 = 4326;

    private static final double insertedLatitude = -30;
    private static final double insertedLongitude = 50;

    private static Connection con;

    @BeforeAll
    private static void setUpDatabase()
    throws SQLException
    {
        createConnection();
        createSchema();
        insertTestDataUsingGeographyObjects();
    }

    @AfterAll
    private static void closeDatabase()
    throws SQLException
    {
        if (con != null)
            con.close();
    }

    @BeforeEach
    private void cleanAndReinsertData()
    throws SQLException
    {
        try (PreparedStatement stat = con.prepareStatement("delete from geotest")) {
            stat.executeUpdate();
        }

        insertTestDataUsingGeographyObjects();
    }

    private static void createConnection()
    throws SQLException
    {
        String uri = System.getProperty("jdbc.url");
        String user = System.getProperty("jdbc.user");
        String password = System.getProperty("jdbc.password");
        con = DriverManager.getConnection(uri, user, password);
    }

    private static void createSchema()
    throws SQLException
    {
        try (PreparedStatement stat = con.prepareStatement("create table geotest (id integer not null primary key, location geography not null)"))
        {
            stat.executeUpdate();
        }
    }

    /**
     * Insert test data using Geography objects.
     */
    private static void insertTestDataUsingGeographyObjects()
    throws SQLException
    {
        System.out.println("Inserting geo-point with latitude=" + insertedLatitude + ", longitude=" + insertedLongitude);
        try (ISQLServerPreparedStatement stat = con.prepareStatement("insert into geotest (id, location) values (?, ?)").unwrap(ISQLServerPreparedStatement.class))
        {
            stat.setInt(1, 1);
            stat.setGeography(2, Geography.point(insertedLatitude, insertedLongitude, WGS_84));
            stat.executeUpdate();
        }
    }

    @Test
    void readLatLongUsingGeographyObject()
    throws SQLException
    {
        try (ISQLServerPreparedStatement stat = con.prepareStatement("select location from geotest").unwrap(ISQLServerPreparedStatement.class);
             ISQLServerResultSet rs = stat.executeQuery().unwrap(ISQLServerResultSet.class))
        {
            rs.next();
            Geography g = rs.getGeography(1);
            System.out.println("Select with geography object: latitude=" + g.getLatitude() + ", longitude=" + g.getLongitude());
            assertThat(g.getLatitude()).describedAs("geography object's latitude").isCloseTo(insertedLatitude, DELTA);
            assertThat(g.getLongitude()).describedAs("geography object's longitude").isCloseTo(insertedLongitude, DELTA);
        }
    }

    @Test
    void readLatLongUsingDatabaseFunctions()
    throws SQLException
    {
        try (ISQLServerPreparedStatement stat = con.prepareStatement("select location.Lat, location.Long from geotest").unwrap(ISQLServerPreparedStatement.class);
             ISQLServerResultSet rs = stat.executeQuery().unwrap(ISQLServerResultSet.class))
        {
            rs.next();
            double latitude = rs.getDouble(1);
            double longitude = rs.getDouble(2);
            System.out.println("Select with database function: latitude=" + latitude + ", longitude=" + longitude);
            assertThat(latitude).describedAs("database function's latitude").isCloseTo(insertedLatitude, DELTA);
            assertThat(longitude).describedAs("database function's longitude").isCloseTo(insertedLongitude, DELTA);
        }
    }

    @Test
    void canInsertLatLongCloseToMaxValuesUsingGeography()
    throws SQLException
    {
        double bigLatitude = 89; //Just in range for a latitude
        double bigLongitude = 179; //Just in range for a longitude
        System.out.println("Inserting geo-point with latitude=" + bigLatitude + ", longitude=" + bigLongitude);
        try (ISQLServerPreparedStatement stat = con.prepareStatement("insert into geotest (id, location) values (?, ?)").unwrap(ISQLServerPreparedStatement.class))
        {
            stat.setInt(1, 2);
            stat.setGeography(2, Geography.point(bigLatitude, bigLongitude, WGS_84));
            stat.executeUpdate();
        }
    }
}
